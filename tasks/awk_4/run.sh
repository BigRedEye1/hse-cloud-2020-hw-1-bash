#!/usr/bin/env bash

awk '{printf("%s", $0); if (NR % 2 == 0) { printf("\n"); } else { printf(";"); }}'
