#!/usr/bin/env bash

read X
read Y
read Z

if (( $X == $Y )) && (( $Y == $Z )); then
    echo -n EQUILATERAL
elif (( $X == $Y )) || (( $Y == $Z )); then
    echo -n ISOSCELES
else
    echo -n SCALENE
fi
