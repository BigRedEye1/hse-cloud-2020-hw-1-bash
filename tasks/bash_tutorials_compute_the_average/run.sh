#!/usr/bin/env bash


SUM=0

read N
for i in `seq 1 $N`; do
    read number
    SUM=$(( $SUM + $number ))
done

printf "%.3f\n" `bc -l <<< "$SUM / $N"`
