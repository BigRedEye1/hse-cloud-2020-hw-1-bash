#!/usr/bin/env bash

readarray -t countries
declare -a filtered=( ${countries[@]/*[aA]*/} )
echo ${filtered[@]}
