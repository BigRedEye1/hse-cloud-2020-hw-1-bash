#!/usr/bin/env bash

uniq -c | sed -r 's/^\s+//g' | cut -d' ' -f2,1 | head -c-2
