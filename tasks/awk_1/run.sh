#!/usr/bin/env bash

awk '{if (NF < 4) { printf("Not all scores are available for %s\n", $1) }}'
